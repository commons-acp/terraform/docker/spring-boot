data "atn-utils_gitlab_package" "jar" {
  repository_url = var.gitlab_package_url
  access_token = var.gitlab_api_token
  output_path = "${abspath(path.root)}/${var.app_zip_path}"
}

