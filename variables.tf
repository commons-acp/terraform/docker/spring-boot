variable "docker_host" {
  description = ""
}
variable "docker_image" {
  description = ""
}
variable "container_name" {
  description = ""
}
variable "app_zip_path" {
  description = ""
}
variable "aws_region" {
  description = ""
}

variable "app_data_path" {
  description = ""
}

variable "tfstates_bucket" {
  description = "TF states buckets"
}

variable "tfstates_network_allence_key" {
  description = "TF states network  key"
}
variable "config_properties_path" {
  description = ""
}

variable "app_version" {
  description = " version"
}

variable "back_artifact_id" {
  description = "jar artifact  id in nexus"
}
variable "gitlab_package_url" {
  description = "gitlab package url"
}
variable "gitlab_api_token" {
  description = "gitlab api token"
}
variable "java_virtual_host" {
  description = "java virtual host"
}
variable "network_name" {
  description = "nom du conteneur"

}
variable "container_path" {
  description = "container path"
}