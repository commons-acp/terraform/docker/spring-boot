resource "docker_image" "java_container_image" {
  name          = data.docker_registry_image.java.name
  pull_triggers = [data.docker_registry_image.java.sha256_digest]
  keep_locally  = true
}

resource "docker_container" "java_container" {
  depends_on = [data.atn-utils_gitlab_package.jar]
  image = docker_image.java_container_image.latest
  name  = var.container_name
  upload {
    file   = "/usr/share/myservice/myservice.jar"
    source = var.app_zip_path
  }

  upload {
    file = "/usr/share/myservice/config.properties"
    source = var.config_properties_path
  }
  volumes {
    host_path = var.app_data_path
    container_path = var.container_path
    read_only = false
  }

  ##### Pour tester en local #####
//  ports {
//    internal = 8080
//    external = 8080
//  }


  ports {
    internal = 8080
    ip = "127.0.0.1"
  }

  ports {
    internal = 80
    ip = "127.0.0.1"
  }

  env = [
    "VIRTUAL_HOST=${var.java_virtual_host}",
    "VIRTUAL_PORT=8080",
    "VIRTUAL_PROTO=http",
    "CERT_NAME=${var.java_virtual_host}",
    "LETSENCRYPT_HOST=${var.java_virtual_host}",

    "VERSION_PROJECT=${var.app_version}",
    "ARTIFACT_ID_BACK=${var.back_artifact_id}",
    "contextConfigurationPath=/usr/share/myservice/config.properties",
  ]


  networks_advanced {
    name = var.network_name
  }



}
