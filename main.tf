terraform {
  required_providers {
    docker = {
      "source": "terraform-providers/docker"
    }
    atn-utils = {
      source = "allence-tunisie/atn-utils"
    }
  }
}
data "docker_registry_image" "java" {
  name = var.docker_image
}
